(defpackage :dag
  (:use :common-lisp
        :closer-common-lisp-user)
  (:export

   :graph
   :make-graph
   :ensure-data
   :vertexes

   :dag
   :make-dag

   :serialize

   :vertex
   :make-vertex
   :ensure-vertex
   :ensure-connected-vertices
   :connection-list

   :edge
   :make-edge
   :edge-p
   :ensure-edge
   :add-edge
   :ensure-edge-in-graph

   :dag-vertex
   :make-dag-vertex

   :find-data
   :walk
   :locate-data-paths
   :find-paths-from))

(in-package :dag)
(use-package :generic-comparability)

(genhash:register-hash-function 'generic-comparability:equals
                                #'generic-comparability:hash-code
                                #'generic-comparability:equals)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun induce (list func)
  "Map over list recursively, calling func on each atom found. Returns
the transformed list."
  (loop for e in list
        collect (if (listp e)
                    (induce e func)
                    (funcall func e))))

;; unused. Possibly buggy.
(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
    #'(lambda (&rest args)
        (multiple-value-bind
              (result exists)
            (gethash args cache)
          (if exists
              result
              (setf (gethash args cache)
                    (apply fn args)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric add-edge (v1 v2)
  (:documentation "Adds an edge between vertexes v1 and v2; no
  duplicate detection performed"))

(defgeneric ensure-edge-in-graph (g src dest)
  (:documentation "Ensures an edge between src and dst datapoints in
  `g`; if neither datapoints are in vertexes, they are created; if no
  edge exists, it is created."))

(defgeneric ensure-data (g data)
  (:documentation "Ensures that `data` exists in `g`, creating a
  standalong vertex if required."))

(defgeneric serialize (object stream)
  (:documentation "Recursively serialize object out to stream"))

(defmethod :around serialize (object stream)
  (let ((*print-readably* t))
    (call-next-method)))

;; dispatcher for serialzable things.
(defclass serializable () ())

(defmethod serialize (object stream)
  ;; toss to FORMAT and hope
  (format stream "~s" object))

(defmethod serialize ((object sequence) stream)
  (map 'list
       #'(lambda (ele)
           (format stream "~s" ele)) object))

(defmethod serialize ((object serializable) stream)
  (format stream
          "~s"
          (serialized-structure object)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass vertex
    (serializable)
  ((data
    :accessor data
    :initarg :data
    :initform nil
    :documentation "da datas")
   (connection-list
    :accessor connection-list
    :initarg :nexts
    :initform nil
    :documentation "The edges that this vertex connects to.")))

(setf (fdefinition 'nexts) #'connection-list)

(defmethod generic-comparability:equals ((v1 vertex) (v2 vertex) &rest _)
  (declare (ignore _))
  (generic-comparability:equals (data v1) (data v2)))

(defun make-vertex (data)
  (make-instance 'vertex :data data))

(defmethod print-object ((v vertex) stream)
   (print-unreadable-object (v stream :type t :identity t)
     (format stream "d: ~a" (data v))))



(defun class-slots-symbols (class-instance)
  "Returns a list of the symbols used in the class slots"
  (mapcar 'closer-mop:slot-definition-name
          (closer-mop:class-slots
           (class-of class-instance))))

(defgeneric serialized-structure (object))
(defmethod serialized-structure (object)
  ;; we don't know how to handle this, and we won't.

  object)
(defmethod serialized-structure ((instance serializable))
  (let ((clazz-name (class-name (class-of instance)))
         (slots (class-slots-symbols instance)))
    `((instance ,clazz-name)
      (slots
       ,(loop for slot in slots
              collect
              (cons slot
                    (serialized-structure
                     (slot-value instance slot))))))))

(defclass edge
      (serializable)
    ((vertex1
      :accessor vertex1
      :initarg :vertex1
      :initform nil
      :documentation "Vertex 1")
     (vertex2
      :accessor vertex2
      :initarg :vertex2
      :initform nil
      :documentation "Vertex 2")
     (properties
      :accessor properties
      :initarg :properties
      :initform nil
      :documentation "List of properties the edge has. Expressed as an
   alist")))

(defmethod equals ((e1 edge) (e2 edge) &rest _)
  (declare (ignore _))
  (and
   (equals (vertex1 e1) (vertex1 e2))
   (equals (vertex1 e1) (vertex1 e2))))

(defmethod print-object ((e edge) stream)
  (print-unreadable-object (e stream :type t :identity t)
    (format stream "")))

(defun make-edge ()
  (make-instance 'edge))

(defmethod edge-p ((src vertex) (dest vertex))
  "Returns true if `src` and `dest` have an edge betwen them."
  (member dest (nexts src) :test #'equals))


(defmethod add-edge ((v1 vertex) (v2 vertex))
  "Adds a two-way edge between `v1` and `v2`."
  (let ((edge
          (make-instance 'edge :vertex1 v1 :vertex2 v2)))
    (push edge (connection-list v1))
    (push edge (connection-list v2))))

(defmethod ensure-edge ((src vertex) (dest vertex))
  "Ensures an edge exists between the previously existing `src` and
`dest` vertexes."
  (unless (edge-p src dest)
    (add-edge src dest)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass dag-vertex (vertex)
  ()
  (:documentation "specialized functions for dag nodes; dag-vertexes
  only have edges that run one-way"))

(defun make-dag-vertex (data)
  (make-instance 'dag-vertex :data data))

(defmethod add-edge ((v1 dag-vertex) (v2 dag-vertex))
  "Adds a one-way edge between v1 and v2"
  (let ((edge
          (make-instance 'edge :vertex1 v1 :vertex2 v2)))
    (push edge (connection-list v1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass graph (serializable)
  ((vertexes
    :accessor vertexes
    :initarg :vertexes
    :initform nil)))

(defun make-graph () (make-instance 'graph))

(defmethod ensure-vertex ((graph graph) (vertex vertex))
  "Adds a perhaps disconnected `vertex` to `graph`. Don't call this
unless you want a forest."
  (flet ((%insert (graph vertex)
           (car (push vertex (vertexes graph)))))

    (anaphora:aif (member vertex (vertexes graph) :test #'equals)
         it
         (%insert graph vertex))))

(defmethod alloc-typed-vertex ((graph graph) data)
  (make-vertex data))


(defmethod ensure-data ((graph graph) data)
  (alexandria:if-let
      ((vertex-p (find-data graph data)))
    vertex-p
    (ensure-vertex graph
                   (alloc-typed-vertex graph data))))

(defmethod ensure-edge-in-graph ((g graph) src dest)
  "For graph `g`, ensure a vertex `src` and `dest` exist, with an edge between them."
  (let ((src-vertex (or (find-data g src)
                        (ensure-data g src)))
        (dest-vertex (or (find-data g dest)
                         (ensure-data g dest))))
    (ensure-edge src-vertex dest-vertex)))

(defmethod look-into ((g graph) data (v vertex))
  "Look into graph `g` for `data`, starting at vertex `v`"
  (labels
      ((look-into-internal (g data v &optional path-end-function path)
         "Seek recursively down from v, looking for data. When the leaf is
reached, `path-end-function` is called."
         (cond ((equals
                 (data v)
                 data)
                (funcall path-end-function (cons v path)))
               (t
                (loop for v-n in (nexts v)
                      do
                      (look-into-internal g data (vertex2 v-n)
                                          path-end-function (cons v path)))))))
      (let* ((path-list)
             (path-list-appender
               #'(lambda (list)
                   (push list path-list))))
     (look-into-internal g data v path-list-appender)
     path-list)))

(defmethod find-paths-from ((g graph) (v vertex))
  "Look into graph `g` , starting at vertex `v`"
  (labels
      ((look-into-internal (g v &optional path-end-function path)
         "Seek recursively down from v When the leaf is
reached, `path-end-function` is called."
         (cond
          ((null (connection-list v))
           (funcall path-end-function (cons v path)))
          (t
           (loop for v-n in (connection-list v)
               do
               (look-into-internal g (vertex2 v-n) path-end-function (cons v path)))))))

      (let* ((path-list)
             (path-list-appender
               #'(lambda (list)
                   (push list path-list))))
        (look-into-internal g v path-list-appender)
        (mapcar #'reverse path-list))))


(defmethod locate-data-linearly ((g graph) data)
  "Perform a O(n) search on graph `g` of size n for `data`, using EQUALS.
Returns the vertex containing `data` or NIL."
  (loop for v in (vertexes g)
        when (equals (data v) data) return v))

;; because... DERP.
(setf (fdefinition 'find-data) #'locate-data-linearly)

(defmethod locate-data-paths-from ((g graph) start end)
  (look-into g end (find-data g start)))

(defmethod locate-data-paths ((g graph) data)
  "find all paths that lead to data"
  (loop for v in (vertexes g)
        append
        (look-into g data v)))

(defmethod walk ((g graph) func start-vertex)
  "Walks g, starting from `start-vertex`, calling `func`. If `g` is
mutated, `walk` may not operate correctly. If a cycle exists, the
`walk` terminates immediately on detection."
  (let ((seen-vertexes nil))
    (labels
        ((inner-function (g func current-vertex)
           (when
               (member current-vertex seen-vertexes :test #'equals)
             (return-from walk nil))
           (push current-vertex seen-vertexes)
           (funcall func current-vertex)
           (loop for edge in (connection-list current-vertex)
                 do (inner-function g func (vertex2 edge)))))
      (inner-function g func start-vertex))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass dag (graph) ())

(defun make-dag () (make-instance 'dag))

(defmethod alloc-typed-vertex ((graph dag) data)
  (make-dag-vertex data))


(defmethod ensure-connected-vertices ((g graph) (list-of-vertexes list))
  (let ((len (length list-of-vertexes)))
    (loop for i from 0 below len
          do
          (if (< (1+ i) len)
              (ensure-edge-in-graph g (elt list-of-vertexes i)
                                    (elt list-of-vertexes (1+ i)))
            (ensure-data g (elt list-of-vertexes i))))))
