;;;; trees, bro.
(defpackage :trees
  (:use :common-lisp)
  ;; figure out shadowing for delete, find, and a few others.
  (:export
   :node
   :binary-node
   :pure-binary-node
   :red-black-node
   :tree
   :binary-tree
   :children
   :leaf-p
   :render-tree
   :insert
   :delete-info
   :find-info
   :find-minimum
   :find-maximum
   :find-successor
   :find-left-motion
   ))
(in-package :trees)

(defun listize (ele)
  (when ele
    (list ele)))

;;; nodes

(defclass node
    ()
  ((data :accessor data
         :initarg :data
         :initform nil)

   ;; This is sort of optional.
   (parent :accessor parent
           :initarg :parent
           :initform nil)

   (children :accessor children
             :initarg :children
             :initform nil)

   ;; defines a node's comparability
   (testg :reader testg
         :initarg :testg
         :initform #'<)

   ;; defines a node's equality
   (equalg :reader equalg
           :initarg :equalg
           :initform #'eql)))

(defclass binary-node
    (node)
  ;; Less than right!
  ((left :accessor left
         :initarg :left
         :initform nil)
   (right :accessor right
         :initarg :right
         :initform nil))
  (:documentation "A traditional binary tree node"))

(defclass pure-binary-node
    (binary-node)
  ()
  (:documentation "A write-only binary tree node"))

(defclass red-black-node
    (node)
  ((color :accessor color
          :initarg :color
          :initform 'black)))

;;; trees
(defclass tree
    ()
    ((root :accessor root
           :initarg :root
           :initform nil)))

(defclass binary-tree
    (tree)
  ()
  (:documentation "A traditional binary search node"))

(defmethod children ((node binary-node))
  (append  (listize (left node))
           (listize (right node) )))

(defmethod (setf children) (data (node binary-node))
  (error "Unable to directly SETF children of a binary node"))

(defmethod leaf-p ((node node))
  (equalg (children node) nil))

(defmethod leaf-p ((node binary-node))
  (equalg (children node) nil))

(defmethod render-tree ( (node tree) &optional (depth 0))
  (when (root node)
    (render-tree (root node) depth)))

(defmethod render-tree ( (node node) &optional (depth 0))
  (format t "~a data ~a~&"
          (format nil "~{~a~}"
                  (loop for n from 1 upto depth collect #\Tab))
          (data node))
  (loop for child in (children node) do
       (if child
         (render-tree child (1+ depth))
         (format t "! ~%"))))

(defmethod render-tree ((node binary-node) &optional (depth 0))
  (let ((spacing (format nil "~{~a~}"
                         (loop for n from 1 upto depth collect #\Tab))  ))
    (format t "~a~a~%"
            spacing
            (data node))

    (if (right node)
        (render-tree (right node) (1+ depth))
        (format t "~aR! ~%" spacing))

    (if (left node)
        (render-tree (left node) (1+ depth))
        (format t "~aL! ~%~%" spacing))))

(defmethod print-object ((obj binary-node) stream)
   (print-unreadable-object (obj stream :type t :identity t)
     (princ (data obj) stream)))

(defgeneric insert (data root))

(defmethod insert (data (node binary-tree))
  (if  (root node)
       ;; Usual case
       (insert (make-instance 'binary-node :data data)
               (root node))
       ;; initial insert
       (setf (root node) (make-instance 'binary-node :data data)))
  node)

(defmethod insert (data (node pure-binary-node))
  (flet ((safe-traverse (data node)
           "Ensures that we don't walk into a null node - if we were
going to, we create a new leaf instead and return it"
           (if node
               node
                (make-instance 'pure-binary-node :data data))))
    (if (not (data node))
        (make-instance 'pure-binary-node :data data)
        (cond ((equalg data (data node))
           ;; don't dupe
           node)
          ((testg data (data node))
            (make-instance 'pure-binary-node
                         :data (data node)
                         :left (insert data (safe-traverse data (left node)))
                         :right (right node)))
          ((not (testg data (data node)))
           (make-instance 'pure-binary-node
                          :data (data node)
                          :left (left node)
                          :right (insert data (safe-traverse data (right node)))))))))

(defmethod insert ((data t) (root binary-node) )
  (insert (make-instance 'pure-binary-node :data data) root))

(defmethod insert ((newnode binary-node) (node binary-node))
  (if (funcall eql (data newnode) (data node))
      node
    (if (testg
         (data newnode) (data node))

        ;; Left
        (progn
          (cond
            ((left node)
             (insert newnode (left node)))
          (t
           (setf (left node) newnode)
           (setf (parent newnode) node))))


        ;; Right
        (progn
          (cond
            ((right node)
             (insert newnode (right node)))
            (t
             (setf (right node) newnode)
             (setf (parent newnode) node)))))))

(defgeneric delete-info (data node)
  (:documentation "Delete data from the tree node specified. Ordering
  tests are performed by `:test`, equality tests are performed by
  `:eql`

Returns the tree object that was originally passed in."))

(defmethod delete-info ((data t) (tree binary-tree))
  ;; The entire tree might be rotated. There are 3 options: (1) the
  ;; tree is size 1 and we deleted the root; (2) we deleted the root
  ;; of tree size > 1; (3) deleted something deep down.
  ;;
  ;; We'll define these cases in the underlying code to have return code NIL,
  ;; node-pointer, and T.
  ;;
  ;; Note, however, for non-T cases, we can assign the root to be the
  ;; return.  For T cases, we don't care!
  (let ((result (delete-info data (root tree))))
    (when (not (eq result t))
        (setf (root tree) result)))
  tree)

(defmethod delete-info ((data t) (node binary-node))
  (let*
      ;; Find the node we are removing
      ((targeted-node (find-info data node))
       (parent-node (parent targeted-node)))

    (cond

      ;; couldn't find the node to delete!
      ((not targeted-node)
       (error "Unable to find ~a to delete" data))

      ;; Zero children
      ((leaf-p targeted-node)
       (if (parent targeted-node)
           ;; if it's not a root
           ;; tell the parent that this child is cut.
           (progn
             (when (equalg (right parent-node)
                        targeted-node)
               (setf (right parent-node)
                     nil))

             (when (equalg (left (parent targeted-node))
                        targeted-node)
               (setf (left (parent targeted-node))
                     nil))
             t)

           ;; Return nil; the tree is now gone.
           nil))

      ;; One child
      ((= (length (children targeted-node)) 1)
       (if (parent targeted-node)
           ;; if it's not a root
           ;; set the parent's child to the child of the target
           (progn
             (when (equalg (right (parent targeted-node))
                        targeted-node)
               (setf (right (parent targeted-node))
                     (car (children targeted-node))))

             (when (equalg (left (parent targeted-node))
                        targeted-node)
               (setf (left (parent targeted-node))
                     (car (children targeted-node))))
             ;; return t; not a root, tree doesn't care.
             t)

           ;; if it's a root then return the node of the child so that
           ;; the tree object can grab it and set it
           (progn
             (first (children targeted-node)))))


      ;; Two children
      ((= (length (children targeted-node)) 2)
       (let ((succ (find-minimum (right targeted-node))))
         ;; set the current node's data to be the data of successor.
         (setf (data targeted-node) (data succ))
         ;; and delete successor from the tree
         (delete-info (data succ) succ)
         t))

      (t
       (format t "~S~&" (children targeted-node))
       (error "Impossible...")))))

(defgeneric find-info (data  tree )
  (:documentation "Search through a tree for data; test is a
  comparison test; eql is the equality test for the datatype"))

(defmethod find-info (data (root tree))
  (find-info data (root root)))

(defmethod find-info (data (root binary-node))
  "Search through a tree for `data`, starting from `root`. Returns the
data or nil"
  ;; got it!
  (when (equalg data (data root))
      (return-from find-info root))

  ;; keep searching
  (if (testg data (data root))
      (if (left root)
          (find-info data (left root))
          nil)
      (if (right root)
          (find-info data (right root))
          nil)))

;; the hey is this?
(defun tack (list ele)
  (assert (listp list))
  (if (null list)
      (list ele)
      (append list  (cons ele nil))))

(defmethod find-info-with-path (data (node binary-node) caller &key test eql)
  "Search through a tree for `data`, starting from `node`. Returns two
values: the data and the path or nil"

  (if (equalg data (data node))
      ;; Got it!
      (values node caller)

      ;; Keep searching; IF is used to branch
      (if (testg data (data node))
	  (when (left node)
	      (find-info-with-path data (left node)
				   (tack caller node)))
	  (when (right node)
	      (find-info-with-path data (right node)
				   (tack caller node))))))

(defgeneric find-minimum (node))
(defmethod find-minimum ((node binary-node) )
  "Minimum is leftmost"
  ;; We're at the end. .
  (if (or (leaf-p node)
          (not (left node)))
      node
      (find-minimum (left node))))
(defgeneric find-maximum (node))
(defmethod find-maximum ((node binary-node) )
  "Maximum is rightmost"
  ;; We're at the end. .
  (if (or (leaf-p node)
          (not (right node)))
      node
      (find-maximum (right node))))

;; tested - Fri Oct 19 16:27:06 PDT 2012
(defmethod find-successor ((node binary-node))
  "Finds the one node that's bigger than `node`"
  (if (right node)
      ;; find the minimum of the large side
      (find-minimum (right node))
      (find-left-motion node)))

;; I have no idea what this drek is.
(defun find-left-motion (node)
  (if (parent node)
      (if (equalg (left (parent node)) node)
          (parent node)
          (find-left-motion (parent node)))
      nil))


(defun generate-tree ()
  (let ((new-tree (make-instance 'binary-tree)))
    (insert 0 new-tree)
    (insert -5 new-tree)
    (insert 5 new-tree)
    (insert 10 new-tree)
    (insert 7 new-tree)
    (insert -10 new-tree)
    (insert -8 new-tree)
    (insert -13 new-tree)
    (insert -15 new-tree)
    (insert 12 new-tree)
    (insert -2 new-tree)
    new-tree))

(defun generate-tree-from-list (list)
  (let ((new-tree (make-instance 'binary-tree)))
    (loop for var in list do
         (insert var new-tree))
    new-tree))

(defun generate-random-tree (n)
  (let  ((new-tree (make-instance 'binary-tree)))
    (loop for counter from 0 upto n
       do (insert (- (random 100) 50) new-tree))
    new-tree))

(defun generate-random-pure-tree (n)
  (let  ((new-tree (make-instance 'pure-binary-node)))
    (loop for counter from 0 upto n
       do (setf new-tree
                (insert (- (random 100) 50) new-tree)))
    new-tree))

;(validate-red-black-properties (make-instance 'red-black-tree))
;(root (make-instance 'red-black-tree :root '(10 2)))
;(describe (root (make-instance 'red-black-tree)))



(defclass red-black-tree
    (binary-tree)
  ((root
    :initform (make-instance 'red-black-node))))

(defclass red-black-node
    (binary-node)
  ((color ;should take 'black or 'red
    :accessor color
    :initarg :color
    :initform 'black)) )

;; after okasaki
(defun fork (color data rbtree-left  rbtree-right)
  (make-instance
   'red-black-node
   :color color
   :left rbtree-left
   :right rbtree-right))

(defun leaf ()
  (make-instance 'red-black-node))

;;todo- turn into a flet
(defun ins ()
  )

;; assumption: numerical (for now)
(defmethod insert (data (node red-black-node))
  (fork 'black ))


(defmethod validate-red-black-properties ((tree red-black-tree))

  ;; Derived from the following web page:
  ;; http://www.cs.auckland.ac.nz/software/AlgAnim/red_black.html
  ;;
  (let ((root-node (root tree)))
    ;; These are node-level verifications
    (and (validate-red-black-properties root-node)
         (validate-black-height-property root-node))))
(defmethod validate-black-height-property ((node red-black-node))
  )

(defmethod validate-red-black-properties ((node red-black-node))
  (and

   ;; First ensure that we have a right color
   (member (color node) '(red black) :test #'eq)

   ;; Validate no red nodes adjacent
   (if (eq (color node) 'red)
       (equal (children node) '(black black))
       ;; if it's a black node, it doesn't factor into this test
       t)

   ;; Recursive step
   (every #'identity
    (mapcar
     #'(lambda (n)
         (cond ((typep n 'red-black-node)
                (validate-red-black-properties n))
               ;; This is OK, it's a "black" node
               ((not n)
                t)
               (t
                (describe n)
                (error "Unable to grasp this node-type: ~a" n))))
     (children node)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Heap big tree.
;;;
;;; Optimizations:
;;;
;;; Remove the defmethod and replace with defun.
;;; Remove the (funcall (test heap)...)
;;;
;;; those two will produce about a 15% speedup on sbcl 1.1.4 on mass
;;; insert.

(defclass heap ()
  ((size :accessor size
         :initarg :size
         :initform 0)

   (data :accessor data)

   ;; Use for preallocation.
   (initsize :reader initsize
             :initarg :initsize
             :initform 1)
   (growth-scale :accessor growth-scale
                 :initarg :growth-scale
                 ;; initform 2 has tested reasonably better than 1.1,
                 ;; 1.7, 3, and 4.
                 :initform 2 )

   ;; node test.
   (test :accessor test
         :initarg :test
         :initform #'>)))


(defmethod initialize-instance :after ((heap heap) &rest initargs)
  (let ((data (make-array (list (initsize heap))
                          :adjustable t)))
    (setf (data heap) data)))

(defun make-heap (&key  (test #'>) (growth-scale 1.1) (initsize 1) )
  (make-instance 'heap :test test
                       :initsize initsize
                       :growth-scale growth-scale))

(defun parent-index-of (idx)
  (floor (/ idx 2)))

(defmethod sift-up ((heap heap) idx)
  ;; tail recursive.
  (when (/= idx 0)
    (let ((parent-idx (parent-index-of idx))
          (data (data heap)))
      (when (funcall (test heap)
                     (aref data idx)
                     (aref data parent-idx))
        ;; swap
        (rotatef (aref data idx) (aref data parent-idx))
        (sift-up heap parent-idx)))))

(defmethod insert ((heap heap) thing)
  (let ((size (size heap)))
    (when (= (length (data heap))
             size)
      ;; ceiling for the first iteration, truncation used after that
      ;; to good effect.
      (adjust-array (data heap) (list (ceiling (* (growth-scale heap) size)))))
    (setf (aref (data heap) size) thing)
    (incf (size heap))
    (sift-up heap size)
    heap))
