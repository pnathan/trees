;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(asdf:defsystem
 #:trees
 :depends-on
 (#:alexandria
  #:anaphora
  #:closer-mop
  #:generic-comparability
  #:genhash
  #:impl-hash)
  :components ((:file "dag")
               (:file "trees"))
  :name "trees"
  :version "1"
  :author "Paul Nathan"
  :maintainer "Paul Nathan"
  :license "AGPL3"
  :description "trees"
  :long-description "trees, man.")
